/**
 * Server start script
 */
// tslint:disable:no-backbone-get-set-outside-model no-var-requires
import * as http from 'http';
import {Server as HttpServer} from 'http';
import {App} from './app';


process.title = 'Datamundi_API';
App.set('port', normalizePort(process.env.PORT || 5000));

process.setMaxListeners(0);
process.on('unhandledRejection', (error) => {
    console.error('Unhandled rejection: ', error);
});

startServer();

function startServer(): HttpServer {
    const application: HttpServer = http.createServer(App);

    console.log('Starting HTTP server...');

    application.listen(App.get('port'));
    application.on('error', onError);
    application.on('listening', onListening);

    process.on('SIGTERM', () => closeSever(application));
    process.on('SIGINT', () => closeSever(application));

    return application;
}

function closeSever(server: HttpServer): void {
    process.exit(0);
}

function normalizePort(val: number | string): number | string | boolean {
    const port: number = (typeof val === 'string') ? parseInt(val, 10) : val;
    if (isNaN(port)) {
        return val;
    } else if (port >= 0) {
        return port;
    }

    return false;
}

function onError(error: NodeJS.ErrnoException): void {
    if (error.syscall !== 'listen') {
        throw error;
    }
    const port: number | string = App.get('port');
    const bind: string = (typeof port === 'string') ? `Pipe ${port}` : `Port ${port}`;

    switch (error.code) {
        case 'EACCES':
            console.error(`${bind} requires elevated privileges`, error);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(`${bind} is already in use`, error);
            process.exit(1);
            break;
        default:
            throw error;
    }
}

function onListening(): void {
    console.log(`Listening on localhost:${App.get('port')}`);
}
