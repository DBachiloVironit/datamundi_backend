import {checkJobExistence, checkUserJobExistence} from './jobExistance';
import {checkJobPermission} from './jobPermission';

export const CheckFreelancer = [checkUserJobExistence];
export const CheckJob = [checkJobExistence];
export const CheckUserJob = [checkUserJobExistence];
