import {NextFunction, Request, Response} from 'express';

const allowedMethods: string[] = ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'];

export const cors = (req: Request, res: Response, next: NextFunction): void => {
    res.header('Access-Control-Allow-Origin', req.headers['origin'] ? req.headers['origin'].toString() : '*');
    res.header('Access-Control-Allow-Headers', 'Accept, Authorization, Content-Type, x-client-version, x-skip-version');

    if (allowedMethods.find((method: string) => req.headers['access-control-request-method'] === method)) {
        res.header('Access-Control-Allow-Method', req.headers['access-control-request-method'].toString());
    }

    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, HEAD, OPTIONS');
    res.header('Access-Control-Allow-Credentials', 'true');

    if (req.method === 'OPTIONS') {
        res.sendStatus(200);
    } else {
        next();
    }
};
