import {NextFunction, Response} from 'express';
import {JobDB} from '../database/JobDB';
import {ResponseProcessor} from '../services/express/ResponseProcessor';
import {IProcessedRequest} from '../shared/models/app';
import {RequestResult} from '../shared/utils/express/RequestResult';


export async function checkJobPermission(req: IProcessedRequest, res: Response, next: NextFunction): Promise<any> {
    let dataSource;
    if (req.method === 'POST') {
        dataSource = req.body;
    } else if (req.method === 'GET') {
        dataSource = req.query;
    }

    const job = dataSource.job;
    const fuid = dataSource.fuid;

    if (req.jobDB instanceof JobDB && await req.jobDB.checkFUID(fuid, job)) {
        next();
    } else {
        req.jobDB.close();
        new ResponseProcessor(req, res).process(new RequestResult('bad_fuid', {}, 400));
    }
}

