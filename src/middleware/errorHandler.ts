import {NextFunction, Request, Response} from 'express';
import {ResponseProcessor} from '../services/express/ResponseProcessor';
import {RequestResult} from '../shared/utils/express/RequestResult';

/**
 * Catch unexpected errors.
 * @param error
 * @param {e.Request} req
 * @param {e.Response} res
 * @param {e.NextFunction} next
 */
export const routerErrorHandler = (error: any, req: Request, res: Response, next: NextFunction): void => {
    if (res.headersSent || (req as any).headersSent) {
        return;
    }

    if (error.hasOwnProperty('result') && error.hasOwnProperty('error')) {
        error = error.error ? error.error : null;
        if (!error) {
            next();
            return;
        }
    }

    if (error) {
        error = new RequestResult(error.errorCode, {
            statusCode: error.statusCode || 500,
            data: error.data || null
        });
        new ResponseProcessor(req, res).process(error);
    }
};
