import {NextFunction, Response} from 'express';
import {JobDB} from '../database/JobDB';
import {ResponseProcessor} from '../services/express/ResponseProcessor';
import {IProcessedRequest} from '../shared/models/app';
import {RequestResult} from '../shared/utils/express/RequestResult';


export async function checkJobExistenceCB(isUser: boolean, req: IProcessedRequest, res: Response, next: NextFunction): Promise<any> {
    let dataSource;
    if (req.method === 'POST') {
        dataSource = req.body;
    } else if (req.method === 'GET') {
        dataSource = req.query;
    }

    let fuid = dataSource.fuid;
    if (!isUser) {
        fuid = undefined;
    }
    const job = dataSource.job;

    try {
        req.jobDB = new JobDB(job, fuid);
        await req.jobDB.connect();

        next()
    } catch (e) {
        new ResponseProcessor(req, res).process(new RequestResult('bad_job_name', {}, 400));
    }
}

export async function checkUserJobExistence(req: IProcessedRequest, res: Response, next: NextFunction): Promise<any> {
    checkJobExistenceCB(true, req, res, next);
}

export async function checkJobExistence(req: IProcessedRequest, res: Response, next: NextFunction): Promise<any> {
    checkJobExistenceCB(false, req, res, next);
}
