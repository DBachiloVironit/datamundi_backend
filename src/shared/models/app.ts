import {Request} from 'express';
import {JobDB} from '../../database/JobDB';


export interface IProcessedRequest extends Request {
    query: any;
    params: any;
    body: any;

    jobDB: JobDB;
}
