import {NextFunction, Response} from 'express';
import {ResponseProcessor} from '../../../services/express/ResponseProcessor';


export declare type ControllerRequestHandler = (req: any, res: Response) => Promise<void>;

/**
 * Util to execute handlers sequence.
 * To use inside Controller function.
 */
export class RequestControllerExecutor {

    private req: any;
    private res: Response;

    private handlers: ControllerRequestHandler[];

    /**
     * Configure execution scope
     *
     * @param req
     * @param {e.Response} res
     * @param {RequestHandler[]} handlers
     */
    constructor(req: any, res: Response, handlers: ControllerRequestHandler[]) {
        if (!handlers || handlers.length === 0) {
            throw Error(`handlers list can't be empty`);
        }

        this.req = req;
        this.res = res;
        this.handlers = handlers;
    }

    /**
     * Execute handler sequence and catch errors. At the end sends latest handler result as response.
     *
     * @returns {Promise<any>}
     */
    public async execute(): Promise<any> {
        let result;
        let error;
        try {
            for (const handler of this.handlers) {
                result = handler(this.req, this.res);

                if (result instanceof Promise) {
                    result = await result;
                }
            }
        } catch (err) {
            error = err;
        } finally {
            try {
                this.send(error, result);
            } catch (e) {
                console.error('Error in RequestControllerExecutor.send', e.code)
            }
        }

        return {error, result};
    }

    /**
     * Send response using ResponseProcessor.
     *
     * @param error
     * @param result
     * @returns {Response}
     */
    public send(error, result): Response {
        const processor = new ResponseProcessor(this.req, this.res);
        if (error) {
            return processor.process(error);
        }
        return processor.process(result);
    }
}
