export class RequestResult {
    code: string;
    data: any;
    statusCode: number;

    constructor(code: string = 'ok', data: any = {}, statusCode: number = 200) {
        this.code = code;
        this.data = data;
        this.statusCode = statusCode;
    }
}
