import {NextFunction, RequestHandler, Response} from 'express';
import {ResponseProcessor} from '../../../services/express/ResponseProcessor';
import {ControllerRequestHandler, RequestControllerExecutor} from './RequestControllerExecutor';


/**
 * Route controller.
 *
 * Example:
 *  > IndexRouter.get('/', Controller([handler1, handler2]));
 *
 *  OR
 *
 *  controller.ts
 *  > export const DefaultController = Controller([handler1, handler2]);
 *
 *  IndexRouter.ts
 *  > IndexRouter.get('/', DefaultController);
 *
 * @param {ControllerRequestHandler[]} handlers
 * @returns {e.RequestHandler}
 */
export default function Controller(handlers: ControllerRequestHandler[]): RequestHandler {
    return (req: any, res: Response, next: NextFunction) => {
        new RequestControllerExecutor(req, res, handlers).execute()
            .catch((error: any) => {
                if (!res.headersSent) {
                    new ResponseProcessor(req, res).process(error);
                } else {
                    console.error('Error after Controller executed', error);
                }
            });
    };
}
