/**
 * Express App initialization
 */

import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as fs from 'fs';
import * as morgan from 'morgan';
import * as path from 'path';
import {cors} from './middleware/cors';
import {routerErrorHandler} from './middleware/errorHandler';
import {router} from './router/index';


class ApplicationLoader {
    public app: express.Application;

    constructor() {
        this.app = express();
        this.middleware();
        this.routes();
    }

    private middleware(): void {
        // this.bootstrap();
        this.app.use(bodyParser.json());
        this.app.use(bodyParser.urlencoded({extended: false}));
        this.app.use(cors);
        this.logging();
    }

    private bootstrap(): void {
        fs.mkdir(path.resolve('./static/jobs'), 0o777, (err) => {
            if (err && err.code !== 'EEXIST') {
                console.error('Error creating folder: ', err);
            } else {
                fs.mkdir(path.resolve('./static/jobs/user'), 0o777, (err) => {
                    if (err && err.code !== 'EEXIST') {
                        console.error('Error creating folder: ', err);
                    }
                })
            }
        });
    }

    private logging(): void {
        const morganMask = ':status :method :url - :response-time ms';
        this.app.use((req, res, next) => {
            return morgan(morganMask, {
                skip() {
                    return res.statusCode >= 400;
                },
                stream: {
                    write: (str: string) => {
                        console.info(str);
                    }
                }
            })(req, res, next)
        });
        this.app.use((req, res, next) => {
            return morgan(morganMask, {
                skip() {
                    return res.statusCode < 400 || res.statusCode >= 500;
                },
                stream: {
                    write: (str: string) => {
                        console.warn(str);
                    }
                }
            })(req, res, next)
        });
        this.app.use((req, res, next) => {
            return morgan(morganMask, {
                skip() {
                    return res.statusCode < 500;
                },
                stream: {
                    write: (str: string) => {
                        console.error(str);
                    }
                }
            })(req, res, next)
        });
    }

    /**
     * Routers binding
     */
    private routes(): void {
        this.app.use('/', router);
        this.app.use(routerErrorHandler);
    }
}

export const App: express.Application = new ApplicationLoader().app;
export default App;
