import {Request, Response} from 'express';
import {RequestResult} from '../../shared/utils/express/RequestResult';


export class ResponseProcessor {
    private req: Request;
    private res: Response;

    constructor(req: Request, res: Response) {
        this.req = req;
        this.res = res;
    }

    public process(data?: (RequestResult | any), status?: number): any {
        if (data instanceof RequestResult) {
            data.statusCode = status || data.statusCode;
            data = ResponseProcessor.removeEmptyFields(data);
        } else if (!data) {
            data = new RequestResult();
        }

        return this.res.status(data.statusCode).json(data);
    }

    private static removeEmptyFields(data: any): any {
        const result = {};
        for (const key in data) {
            if (data.hasOwnProperty(key) && (data[key] === 0 || data[key])) {
                result[key] = data[key];
            }
        }

        return result;
    }
}
