import {Router} from 'express';
import {CheckJob} from '../../middleware/index';
import {JobsRouter} from './jobs/router';
import {GetPINController} from './login/_POST/controller';
import {GetJobObservationsListController} from './observations/_GET/controller';
import {GetJobQualificationsListController} from './qualifications/_GET/controller';
import {HtmlToolRouter} from './tools/html/router';
import {UrlToolRouter} from './tools/url/router';


export const ApiRouter: Router = Router();

ApiRouter.use('/jobs', JobsRouter);
ApiRouter.post('/login', GetPINController);
ApiRouter.get('/observations', CheckJob, GetJobObservationsListController);
ApiRouter.get('/qualifications', CheckJob, GetJobQualificationsListController);
ApiRouter.use('/tools/url', UrlToolRouter);
ApiRouter.use('/tools/html', HtmlToolRouter);
