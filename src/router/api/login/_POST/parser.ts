import {Request, Response} from 'express';


export const parser = async (req: Request, res: Response): Promise<any> => {
    req.body.email = req.body.email || '';
    req.body.pin = req.body.pin || '';
    req.body.job = req.body.job || '';
};
