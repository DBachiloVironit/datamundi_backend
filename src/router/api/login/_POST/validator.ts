import {Request, Response} from 'express';
import {RequestResult} from '../../../../shared/utils/express/RequestResult';


export const validator = async (req: Request, res: Response): Promise<any> => {
    if (req.body.email.length <= 0 || req.body.pin.length <= 0 || req.body.job.length <= 0) {
        throw new RequestResult('bad_data', {}, 400);
    }
};
