import {Request, Response} from 'express';
import * as fs from 'fs';
import * as path from 'path';
import * as request from 'request';
import {configuration} from '../../../../config';
import {JobDB} from '../../../../database/JobDB';
import {RequestResult} from '../../../../shared/utils/express/RequestResult';

function copyFile(source, target, cb) {
    source = path.join(path.resolve(configuration.jobsDir), source + configuration.jobsFileExt);
    target = path.join(path.resolve(configuration.userJobsDir), target + configuration.jobsFileExt);

    let cbCalled = false;

    if (fs.existsSync(target)) {
        done();
        return;
    }

    const rd = fs.createReadStream(source);
    rd.on('error', (err) => {
        done(err);
    });
    const wr = fs.createWriteStream(target);
    wr.on('error', (err) => {
        done(err);
    });
    wr.on('close', (ex) => {
        done(ex);
    });
    rd.pipe(wr);

    function done(err?: any) {
        if (!cbCalled) {
            cb(err);
            cbCalled = true;
        }
    }
}

export const handler = async (req: Request, res: Response): Promise<any> => {
    return new Promise((resolve, reject) => {
        const options = {
            method: 'POST',
            uri: configuration.pinUrl,
            json: true,
            body: {
                MAIL: req.body.email
            }
        };

        request(options, async (error: any, response: any, body: any) => {
            if (body.length > 0 && body[0].PIN === req.body.pin) {
                const fuid = body[0].FUID;
                const job = req.body.job;
                const userJob = `FUID-${fuid}_${job}`;

                copyFile(job, userJob, async (err) => {
                    if (err) {
                        reject(new RequestResult('error_processing_job', {}, 500));
                        return
                    }

                    let jobDB;

                    try {
                        jobDB = new JobDB(job, fuid);
                        await jobDB.connect();

                        await jobDB.saveFUID(fuid);
                        await jobDB.createMetricsTables();
                    } catch (e) {
                        reject(new RequestResult('bad_job_name', {}, 400));
                    }

                    jobDB.close();

                    const data = {fuid, juid: job};
                    resolve(new RequestResult('ok', data));
                });
            } else {
                reject(new RequestResult('need_contact', {}, 400));
            }
        });
    });
};
