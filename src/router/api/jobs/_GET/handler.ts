import {Request, Response} from 'express';
import * as fs from 'fs';
import * as path from 'path';
import {configuration} from '../../../../config';
import {RequestResult} from '../../../../shared/utils/express/RequestResult';


export const handler = async (req: Request, res: Response): Promise<any> => {
    return new Promise((resolve, reject) => {
        fs.readdir(path.resolve(configuration.jobsDir), (err, files) => {
            if (err) {
                resolve(new RequestResult('ok', {jobs: []}));
            } else {
                const jobs = [];
                files.forEach((file) => {
                    if (path.extname(file).toLowerCase() === configuration.jobsFileExt) {
                        jobs.push(file.slice(0, -configuration.jobsFileExt.length));
                    }
                });
                resolve(new RequestResult('ok', {jobs}));
            }
        })
    });
};
