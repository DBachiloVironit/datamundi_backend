import Controller from '../../../../../shared/utils/express/Controller';
import {handler} from './handler';

export const GetJobMetaController = Controller([handler]);
