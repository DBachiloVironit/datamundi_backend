import {Response} from 'express';
import * as fs from 'fs';
import * as path from 'path';
import {configuration} from '../../../../../config';
import {IProcessedRequest} from '../../../../../shared/models/app';
import {RequestResult} from '../../../../../shared/utils/express/RequestResult';


export const handler = async (req: IProcessedRequest, res: Response): Promise<any> => {
    const meta = await req.jobDB.getJobMeta();
    const data = {
        meta,
        config: {
            html: JSON.parse(fs.readFileSync(path.resolve(configuration.htmlToolConfig), {encoding: 'utf-8'}))
        }
    };

    req.jobDB.close();


    return new RequestResult('ok', data);
};
