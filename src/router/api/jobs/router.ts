import {Router} from 'express';
import {CheckJob} from '../../../middleware/index';
import {GetJobsListController} from './_GET/controller';
import {GetJobMetaController} from './meta/_GET/controller';


export const JobsRouter: Router = Router();

JobsRouter.get('/', GetJobsListController);
JobsRouter.get('/meta', CheckJob, GetJobMetaController);
