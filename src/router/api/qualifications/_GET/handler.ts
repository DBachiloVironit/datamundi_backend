import {Response} from 'express';
import {IProcessedRequest} from '../../../../shared/models/app';
import {RequestResult} from '../../../../shared/utils/express/RequestResult';


export const handler = async (req: IProcessedRequest, res: Response): Promise<any> => {
    const qualifications = await req.jobDB.getJobQualifications();
    const data = {qualifications};

    req.jobDB.close();

    return new RequestResult('ok', data);
};
