import {Request, Response} from 'express';
import {RequestResult} from '../../../../../../shared/utils/express/RequestResult';


export const validator = async (req: Request, res: Response): Promise<any> => {
    if ((isNaN(req.body.pairId) || req.body.pairId < 1) ||
        isNaN(req.body.evaluations.enterTime) ||
        isNaN(req.body.evaluations.leaveTime) ||
        isNaN(req.body.evaluations.mouseMiles) ||
        isNaN(req.body.evaluations.scrollStateSource) ||
        isNaN(req.body.evaluations.scrollStateTranslation) ||
        req.body.evaluations.sourceHtml.length === 0 ||
        req.body.evaluations.translationHtml.length === 0) {
        throw new RequestResult('bad_data', {}, 400);
    }
};
