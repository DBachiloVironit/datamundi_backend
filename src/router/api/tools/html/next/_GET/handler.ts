import {Response} from 'express';
import {IProcessedRequest} from '../../../../../../shared/models/app';
import {RequestResult} from '../../../../../../shared/utils/express/RequestResult';


export const handler = async (req: IProcessedRequest, res: Response): Promise<any> => {
    const promises = [];

    promises.push(req.jobDB.saveTimeMetric(req.body.pairId, req.body.evaluations.enterTime, req.body.evaluations.leaveTime));
    promises.push(req.jobDB.saveMouseMetric(req.body.pairId, req.body.evaluations.mouseMiles));
    promises.push(req.jobDB.saveScrollMetric(req.body.pairId, req.body.evaluations.scrollStateSource, req.body.evaluations.scrollStateTranslation));
    promises.push(req.jobDB.saveTokensMetric(req.body.pairId, req.body.evaluations.sourceHtml, req.body.evaluations.translationHtml));

    await Promise.all(promises).catch((error) => {
        console.error('SQLite error: ', error);
    });

    req.jobDB.close();

    return new RequestResult('ok');
};
