import {Request, Response} from 'express';
import {RequestResult} from '../../../../../../shared/utils/express/RequestResult';


export const parser = async (req: Request, res: Response): Promise<any> => {
    if (!req.body.evaluations) {
        throw new RequestResult('bad_data', {}, 400);
    }

    req.body.pairId = parseInt(req.body.pairId, 10);
    req.body.evaluations.enterTime = parseInt(req.body.evaluations.enterTime, 10);
    req.body.evaluations.leaveTime = parseInt(req.body.evaluations.leaveTime, 10);
    req.body.evaluations.mouseMiles = parseInt(req.body.evaluations.mouseMiles, 10);
    req.body.evaluations.scrollStateSource = parseFloat(req.body.evaluations.scrollStateSource);
    req.body.evaluations.scrollStateTranslation = parseFloat(req.body.evaluations.scrollStateTranslation);
    req.body.evaluations.sourceHtml = req.body.evaluations.sourceHtml || '';
    req.body.evaluations.translationHtml = req.body.evaluations.translationHtml || '';
};
