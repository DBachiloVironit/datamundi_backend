import {Router} from 'express';
import {CheckFreelancer} from '../../../../middleware/index';
import {SubmitTokenMetricsController} from './next/_GET/controller';
import {GetJobStateController} from './state/_GET/controller';


export const HtmlToolRouter: Router = Router();

HtmlToolRouter.post('/next', CheckFreelancer, SubmitTokenMetricsController);
HtmlToolRouter.get('/state', CheckFreelancer, GetJobStateController);
