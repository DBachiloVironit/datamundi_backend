import {Response} from 'express';
import {IProcessedRequest} from '../../../../../shared/models/app';
import {RequestResult} from '../../../../../shared/utils/express/RequestResult';


export const handler = async (req: IProcessedRequest, res: Response): Promise<any> => {
    const pairs = await req.jobDB.getJobSet(req.query.page, req.query.size);
    const totalCount = await req.jobDB.getJobSetCount();
    const data = {pairs, totalCount};

    req.jobDB.close();

    return new RequestResult('ok', data);
};
