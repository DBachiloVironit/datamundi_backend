import {Request, Response} from 'express';


export const parser = async (req: Request, res: Response): Promise<any> => {
    req.query.size = parseInt(req.query.size, 10);
    req.query.page = parseInt(req.query.page, 10);
};
