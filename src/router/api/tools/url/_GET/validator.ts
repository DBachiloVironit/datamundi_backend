import {Request, Response} from 'express';
import {RequestResult} from '../../../../../shared/utils/express/RequestResult';


export const validator = async (req: Request, res: Response): Promise<any> => {
    if (req.query.page < 1 || req.query.size < 1) {
        throw new RequestResult('bad_data', {}, 400);
    }
};
