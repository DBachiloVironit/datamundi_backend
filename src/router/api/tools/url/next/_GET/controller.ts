import Controller from '../../../../../../shared/utils/express/Controller';
import {handler} from './handler';
import {parser} from './parser';
import {validator} from './validator';

export const SubmitPairMetricsController = Controller([parser, validator, handler]);
