import {Response} from 'express';
import {IProcessedRequest} from '../../../../../../shared/models/app';
import {RequestResult} from '../../../../../../shared/utils/express/RequestResult';


export const handler = async (req: IProcessedRequest, res: Response): Promise<any> => {
    const state = await req.jobDB.getUserUrlJobState();
    if (state === false) {
        return new RequestResult('job_done');
    }

    const pair = await req.jobDB.getJobPair(state.PairID);
    const data = {pair};

    req.jobDB.close();

    return new RequestResult('ok', data);
};
