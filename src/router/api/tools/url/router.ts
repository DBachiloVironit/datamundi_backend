import {Router} from 'express';
import {CheckFreelancer} from '../../../../middleware/index';
import {GetUrlToolPairsController} from './_GET/controller';
import {SubmitPairMetricsController} from './next/_GET/controller';
import {GetJobStateController} from './state/_GET/controller';


export const UrlToolRouter: Router = Router();

UrlToolRouter.get('/pairs', CheckFreelancer, GetUrlToolPairsController);
UrlToolRouter.post('/next', CheckFreelancer, SubmitPairMetricsController);
UrlToolRouter.get('/state', CheckFreelancer, GetJobStateController);
