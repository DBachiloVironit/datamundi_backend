import {Request, Response} from 'express';
import {RequestResult} from '../../shared/utils/express/RequestResult';

export const handler = async (req: Request, res: Response): Promise<any> => {
    return new RequestResult('not_found', {}, 404);
};
