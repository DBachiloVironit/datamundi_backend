import {Router} from 'express';
import {DefaultController} from './_ALL/controller';

export const DefaultRouter: Router = Router();

DefaultRouter.all('*', DefaultController);
