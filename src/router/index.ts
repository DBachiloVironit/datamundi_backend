import {Router} from 'express';
import {ApiRouter} from './api/router';
import {DefaultRouter} from './default.router';


export const router: Router = Router();

router.use('/api', ApiRouter);
router.all('*', DefaultRouter);
