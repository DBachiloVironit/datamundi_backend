export const configuration: any = {
    pinUrl: 'https://www.datamundi.be/jobs/COMMONDEV/retrievePIN.php',
    jobsDir: './static/jobs/',
    userJobsDir: './static/jobs/user/',
    jobsFileExt: '.upq',
    htmlToolConfig: './static/html-tool-config.json'
};
