export interface BaseDB {
    query(sql: string, queryArgs: any[] | any): Promise<any>;
}