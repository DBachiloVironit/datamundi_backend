import * as sqlite3 from 'sqlite3';
import {RequestResult} from '../../shared/utils/express/RequestResult';
import {BaseDB} from '../_/BaseDB';

export abstract class SQLite implements BaseDB {
    private db = null;
    private dbName: string;

    constructor(dbName: string) {
        this.dbName = dbName;
    }

    public async connect(): Promise<boolean | any> {
        return new Promise((resolve, reject) => {
            this.db = new sqlite3.Database(this.dbName, sqlite3.OPEN_READWRITE, (error) => {
                if (error) {
                    this.db = null;
                    reject(new RequestResult('bad_database_name', {error: 'Cannot open database'}, 400));
                } else {
                    resolve(true);
                }
            });
        });
    }

    public async query(sql: string, queryArgs: any[] | any = []): Promise<any> {
        return new Promise((resolve, reject) => {
            if (this.db !== null && this.db.open) {
                this.db.all(sql, queryArgs, (err, rows) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(rows);
                    }
                });
            } else {
                reject(new RequestResult('bad_database', {error: 'Database is null'}, 400));
            }
        });
    }

    public close() {
        if (this.db !== null) {
            this.db.close();
            this.db = null;
        }
    }
}
