import * as path from 'path';
import {configuration} from '../config';
import {SQLite} from './SQLite';

export class JobDB extends SQLite {
    public jobFile: string;

    constructor(jobFileName: string, fuid?: string) {
        const jobsDir = (fuid) ? configuration.userJobsDir : configuration.jobsDir;
        jobFileName = ((fuid) ? `FUID-${fuid}_` : '') + jobFileName;
        const jobFilePath = path.join(path.resolve(jobsDir), jobFileName + configuration.jobsFileExt);
        super(jobFilePath);
        this.jobFile = jobFileName + configuration.jobsFileExt;
    }

    public async saveFUID(fuid: string): Promise<any> {
        return this.query(`UPDATE [META] SET [Freelancer] = ?;`, [fuid]);
    }

    public async checkFUID(fuid: string, jobName: string): Promise<boolean> {
        const result = await this.query(`SELECT * FROM [META] WHERE [Freelancer] = ? AND [FileName] = ?;`,
            [fuid, jobName + configuration.jobsFileExt]);
        return result.length === 1;
    }

    public async getJobObservations(): Promise<string[]> {
        return this.query(`SELECT * FROM [O];`);
    }

    public async getJobQualifications(): Promise<string[]> {
        return this.query(`SELECT * FROM [Q];`);
    }

    public async getJobSet(page: number, size: number): Promise<any[]> {
        const offset = (page - 1) * size;
        return this.query(
            `SELECT [PairID], [srcText] AS source, [transText] AS translation, srcLang, transLang FROM [JOBSET] LIMIT ?, ?;`,
            [offset, size]
        );
    }

    public async getJobPair(pairId: number): Promise<any> {
        const sql = `SELECT [PairID], [srcText] AS source, [transText] AS translation, srcLang, transLang FROM [JOBSET] WHERE PairID = ?;`;
        const result = await this.query(sql, [pairId]);
        return result[0];
    }

    public async getJobMeta(): Promise<any> {
        const result = await this.query(`SELECT * FROM [META];`);
        return result[0];
    }

    public async getJobSetCount(): Promise<number> {
        const sql = `SELECT COUNT(*) AS count FROM [JOBSET];`;
        const result = await this.query(sql);
        return result[0].count;
    }

    public async getUserUrlJobState(): Promise<any> {
        const sql = `SELECT * FROM [JOBSET] js LEFT JOIN [Qualifications] qu ON qu.JobSetRef = js.PairID
                     WHERE qu.Qualification IS NULL ORDER BY js.PairID LIMIT 1`;
        const result = await this.query(sql);
        if (result.length > 0) {
            return result[0];
        } else {
            return false;
        }
    }

    public async getUserHtmlJobState(): Promise<any> {
        const sql = `SELECT * FROM [JOBSET] js LEFT JOIN [TokensRec] tr ON tr.JobSetRef = js.PairID
                     WHERE tr.JobSetRef IS NULL ORDER BY js.PairID LIMIT 1`;
        const result = await this.query(sql);
        if (result.length > 0) {
            return result[0];
        } else {
            return false;
        }
    }

    public async createMetricsTables(): Promise<any> {
        return Promise.all([
            this.query(
                `CREATE TABLE IF NOT EXISTS [Qualifications]( [JobSetRef] INT REFERENCES [JOBSET]([PairID]), [Qualification] REFERENCES [Q]([QID]))`
            ),
            this.query(
                `CREATE TABLE IF NOT EXISTS [Observations]( [JobSetRef] INT REFERENCES [JOBSET]([PairID]), [Observation] REFERENCES [O]([OID]))`
            ),
            this.query(`CREATE TABLE IF NOT EXISTS [TimeRec]( [JobSetRef] INT REFERENCES [JOBSET]([PairID]), [T1] datetime, [T2] datetime)`),
            this.query(`CREATE TABLE IF NOT EXISTS [MouseRec]( [JobSetRef] INT REFERENCES [JOBSET]([PairID]), [PixelsTraveled] INTEGER DEFAULT 0)`),
            this.query(
                `CREATE TABLE IF NOT EXISTS [ScrollRec]( [JobSetRef] INT REFERENCES [JOBSET]([PairID]), [ScrollSource] REAL DEFAULT 0,
                [ScrollTranslation] REAL DEFAULT 0)`
            ),
            this.query(`CREATE TABLE IF NOT EXISTS [TokensRec]([JobSetRef] INT REFERENCES [JOBSET]([PairId]), [Source] CHAR, [Translation] CHAR)`)
        ]);
    }

    public async saveQualificationMetric(pairId: number, quality: number): Promise<any> {
        return this.query(`INSERT INTO [Qualifications] VALUES (?, ?);`, [pairId, quality]);
    }

    public async saveObservationMetric(pairId: number, observation: number): Promise<any> {
        return this.query(`INSERT INTO [Observations] VALUES (?, ?);`, [pairId, observation]);
    }

    public async saveTimeMetric(pairId: number, enterTime: number, leaveTime: number): Promise<any> {
        return this.query(`INSERT INTO [TimeRec] VALUES (?, ?, ?);`, [pairId, enterTime, leaveTime]);
    }

    public async saveMouseMetric(pairId: number, mouseMiles: number): Promise<any> {
        return this.query(`INSERT INTO [MouseRec] VALUES (?, ?);`, [pairId, mouseMiles]);
    }

    public async saveScrollMetric(pairId: number, scrollSource: number, scrollTranslation: number): Promise<any> {
        return this.query(`INSERT INTO [ScrollRec] VALUES (?, ?, ?);`, [pairId, scrollSource, scrollTranslation]);
    }

    public async saveTokensMetric(pairId: number, source: string, translation: string): Promise<any> {
        return this.query(`INSERT INTO [TokensRec] VALUES (?, ?, ?);`, [pairId, source, translation]);
    }
}
